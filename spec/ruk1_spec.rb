require 'Ruk1'
require 'json'

RSpec.describe Ruk1 do
  if Ruk1::VERSION == '0.1.0' || Ruk1::VERSION == '0.1.1' || Ruk1::VERSION == '0.1.2'
    it 'Test Basket: 001,002,003 apply all promotional_rules' do
      items = [1,2,3]
      promotional_rules = [1,2]
      expected_price = 66.78
      fired_test(items, expected_price, promotional_rules)
    end
    it 'Test Basket: 001,003,001 apply 1st rule' do
      items = [1,3,1]
      promotional_rules = [1]
      expected_price = 36.95
      fired_test(items, expected_price, promotional_rules)
    end
    it 'Test Basket: 001,002,001,003 apply 2rd rule' do
      items = [1,2,1,3]
      promotional_rules = [2]
      expected_price = 73.76
      fired_test(items, expected_price, promotional_rules)
    end
    it 'Test Basket: 001,002,001,003 without promotional_rules' do
      items = [1,2,1,3]
      expected_price = 73.76
      promotional_rules = []
      fired_test(items, expected_price, promotional_rules)
    end
  elsif Ruk1::VERSION == '0.1.3'
    it 'Test Basket: 001,003,001' do
      items = JSON.parse(JSON.generate([
        {product_id: 1, product_name: 'Lavender heart', product_price: 9.25, quantity: 2},
        {product_id: 3, product_name: 'Kids T-shirt', product_price: 19.95, quantity: 1}
        ])
      )
      promotional_rules = [1,2]
      expected_price = 36.95
      expected_quantity = 3
      fired_test_013(items, promotional_rules, expected_price, expected_quantity)
    end
  elsif Ruk1::VERSION == '0.1.4'
    it 'Test Basket: 001,003,001 v0.1.4' do
      items = JSON.parse(JSON.generate([
        {product_id: '001', product_name: 'Lavender heart', product_price: 9.25, quantity: 2},
        {product_id: '003', product_name: 'Kids T-shirt', product_price: 19.95, quantity: 1}
        ])
      )
      promotional_rules = [1,2]
      expected_price = 36.95
      expected_quantity = 3
      fired_test_013(items, promotional_rules, expected_price, expected_quantity)
    end
  end


  def fired_test(items, expected_price, promotional_rules)
    if Ruk1::VERSION == '0.1.0'
      expect(Ruk1::Checkout.total(items)).to eql(expected_price)
    elsif Ruk1::VERSION == '0.1.1'
      co = Ruk1::Checkout.new()
      items.each do |item|
        co.scan(item)
      end
      price = co.total
      expect(price).to eql(expected_price)
    elsif Ruk1::VERSION == '0.1.2'
      co = Ruk1::Checkout.new(promotional_rules)
      items.each do |item|
        co.scan(item)
      end
      price = co.total
      expect(price).to eql(expected_price)
    end
  end

  def fired_test_013(items, promotional_rules, expected_price, expected_quantity)
    co = Ruk1::Checkout.new(promotional_rules)
    items.each do |item|
      item['quantity'].times do
         co.scan(item)
      end
    end
    price = co.total
    quantity = co.total_quantity
    expect(price).to eql(expected_price)
    expect(quantity).to eql(expected_quantity)
  end
end
