module Ruk1
  class Checkout
    def initialize(promotional_rules)
      @total_price = 0
      @total_item_1 = 0
      @promotional_rules = promotional_rules
    end

    def scan(item)
      if item == 1
        @total_price += 9.25
        @total_item_1 += 1
      elsif item == 2
        @total_price += 45
      elsif item == 3
        @total_price += 19.95
      else
        @total_price += 0
      end
    end

    def total
      if(@promotional_rules.include?1)
        if @total_item_1 > 1
          @total_price = @total_price - (@total_item_1*(9.25-8.5))
        end
      end
      if(@promotional_rules.include?2)
        if @total_price > 60
          @total_price = @total_price * 0.9
        end
      end
      total = @total_price
      total.round(2)
    end
  end
end
