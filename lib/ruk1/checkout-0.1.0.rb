module Ruk1
  class Checkout
    def self.total(items)
      total_price = 0
      total_item_1 = 0
      items.each do |item|
        if item == 1
          total_item_1 += 1
          total_price += 9.25
        elsif item == 2
          total_price += 45
        elsif item == 3
          total_price += 19.95
        else
          total_price += 0
        end
      end
      if total_item_1 > 1
        total_price = total_price - (total_item_1*(9.25-8.5))
      end
      if total_price > 60
        total_price = total_price * 0.9
      end
      total_price.round(2)
    end
  end

end
