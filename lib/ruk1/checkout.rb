module Ruk1
  class Checkout
    def initialize(promotional_rules)
      @total_price = 0
      @total_quantity = 0
      @promotional_rules = promotional_rules
    end

    def scan(item)
      if ((@promotional_rules.include?1) && (item['product_id'] == '001') && (item['quantity'] > 1))
        @total_price += 8.5
      else
        @total_price += item['product_price']
      end
      @total_quantity += 1
    end

    def total
      if(@promotional_rules.include?2)
        if @total_price > 60
          @total_price = @total_price * 0.9
        end
      end
      total = @total_price
      total.round(2)
    end

    def total_quantity
      total_quantity = @total_quantity
    end
  end
end
