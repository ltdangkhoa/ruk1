require "ruk1/version"
require "ruk1/checkout"

module Ruk1
  class Error < StandardError; end

  # items = [1,2,1,3,2,1,2,1]
  # promotional_rules = [1,2]
  #
  # co = Ruk1::Checkout.new(promotional_rules)
  # items.each do |item|
  #   co.scan(item)
  # end
  # price = co.total
  #
  # puts "Total price: £#{price}"

end
